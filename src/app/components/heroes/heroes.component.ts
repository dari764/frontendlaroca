import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {
  hereoes : Heroe[] = [];
  constructor( private _heroesService:HeroesService,
            private router : Router
              ) {
   }

  ngOnInit() {
    this.hereoes = this._heroesService.getHereoes();

    // console.log(this.hereoes);
  }
  verHeroe( idx : number ){
     this.router.navigate( [ '/heroe' , idx ] );
  }

}
